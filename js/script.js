function resize() {

}
var Services_length = $('.all-works div.work-cards').length;
function removeHiddenClass() {
    var count = 1
    $('.card-hidden').each(function() {
        if (count <= 8)
            $(this).removeClass('card-hidden');
        count += 1;
    })
    if ( $('.card-hidden').length == 0)
    {
        $('.load-more').hide();
        $('.load-less').show();
    }

}
function addHiddenClass() {
    $('.work-cards').each(function() {
        $(this).addClass('card-hidden');
        $('.load-more').show();
        $('.load-less').hide();
    });
    removeHiddenClass();
}
$(document).ready(function() {
    const minus = $('.quantity__minus');
    const plus = $('.quantity__plus');
    const input = $('.quantity__input');
    minus.click(function(e) {
        e.preventDefault();
        var value = input.val();
        if (value > 1) {
            value--;
        }
        input.val(value);
    });

    plus.click(function(e) {
        e.preventDefault();
        var value = input.val();
        value++;
        input.val(value);
    });
    
    $(".login-reg").click(function() {
        $("#overlaylogin").toggleClass("modal-enabled");
    });
    $(".mobile-login").click(function() {
        $("#overlaylogin").toggleClass("modal-enabled");
        $(".mobile-menu").slideToggle();
    });
    $("#overlaylogin .login .right a#close").click(function() {
        $("#overlaylogin").toggleClass("modal-enabled");
    });

    $("#overlaylogin .login .right .link a").click(function() {
        $("#overlayreg").toggleClass("modal-enabled");
        $("#overlaylogin").toggleClass("modal-enabled");
    });
    $("#overlayreg .register .right .link a").click(function() {
        $("#overlaylogin").toggleClass("modal-enabled");
        $("#overlayreg").toggleClass("modal-enabled");
    });

    $("#overlaylogin .right a.close").click(function() {
        $("#overlaylogin").toggleClass("modal-enabled");
    });
    $("#overlayreg .right a.close").click(function() {
        $("#overlayreg").toggleClass("modal-enabled");
    });

    $("#overlaylogin .zmdi-eye").click(function(){
        $("#overlaylogin .zmdi-eye").hide();
        $("#overlaylogin .zmdi-eye-off").show();
        $("#password").attr("type","text");
    });
    $("#overlaylogin .zmdi-eye-off").click(function(){
        $("#overlaylogin .zmdi-eye-off").hide();
        $("#overlaylogin .zmdi-eye").show();
        $("#password").attr("type","password");
    });

    $("#overlayreg .password .zmdi-eye").click(function(){
        $("#overlayreg .password .zmdi-eye").hide();
        $("#overlayreg .password .zmdi-eye-off").show();
        $("#reg-password").attr("type","text");
    });
    $("#overlayreg .password .zmdi-eye-off").click(function(){
        $("#overlayreg .password .zmdi-eye-off").hide();
        $("#overlayreg .password .zmdi-eye").show();
        $("#reg-password").attr("type","password");
    });

    $("#overlayreg .conf-password .zmdi-eye").click(function(){
        $("#overlayreg .conf-password .zmdi-eye").hide();
        $("#overlayreg .conf-password .zmdi-eye-off").show();
        $("#reg-confpassword").attr("type","text");
    });
    $("#overlayreg .conf-password .zmdi-eye-off").click(function(){
        $("#overlayreg .conf-password .zmdi-eye-off").hide();
        $("#overlayreg .conf-password .zmdi-eye").show();
        $("#reg-confpassword").attr("type","password");
    });

    $("#hamburger").click(function(e) {
        e.preventDefault();
        $(".mobile-menu").slideToggle();
    });
    $('.more-reviews').click(function(e) {
        e.preventDefault();
        $('.load-reviews').slideToggle();
    });

    $("section.profile div.main div.left div.hire-button").click(function(e) {
        e.preventDefault();
        $("#hire_me_popup").slideToggle();
    });
    $("#hire_me_popup div.hire_me_popup form div.button input").click(function(e) {
        e.preventDefault();
        $("#hire_me_popup").slideToggle();
    });

    $("#hire_me_popup div.hire_me_popup form div.middle_content div.location div.tab div.address").click(function(e) {
        e.preventDefault();
        $("div.l-card div.tab div.address div.b-button").slideToggle();
    });

    $("div.l-card div.tab div.address div.b-button div.b-address").click(function(e) {
        e.preventDefault();
        $("#address-po").slideToggle();
    });
    // $("div.l-card div.tab div.address div.b-button div.b-address").click(function(e) {
    //     e.preventDefault();
    //     $("#hire_me_popup").slideToggle();
    // });

    removeHiddenClass();
    $('.load-more').click(function(e) {
        e.preventDefault();
        count = 1;
        $('.card-hidden').each(function() {
            if (count <= 8)
                $(this).slideDown();
            count += 1
        });
        removeHiddenClass();
        // $('.content-design-alt ').show(); 
        $('.content-design').after('<div class="content-design-alt"></div>');
    });

    $('.load-less').click(function(e) {
        e.preventDefault();
        addHiddenClass(); 
        $('.content-design-alt ').hide();   
        $('.card-hidden').slideUp();
    });

    // $('.zmdi-favorite').click(function(e) {
    //     e.preventDefault();
    //     $(this).toggleClass("active");
    // });

    $('.spotlight-Wishlist a.favorite').click(function(e) {
        e.preventDefault();
        $(this).toggleClass("active");
    });
    $('#featured-products span.zmdi-favorite ').click(function(e) {
        e.preventDefault();
        $(this).toggleClass("active");
    });
    $('#Shop-products span.zmdi-favorite ').click(function(e) {
        e.preventDefault();
        $(this).toggleClass("active");
    });
    $('.slick_one').slick({
        dots: true,
        infinite: true,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    $('.product-item-slider').slick({
        dots: true,
        infinite: true,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    $('.slick_category_container').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        slidesToShow: 7,
        slidesToScroll: 7,
        pauseOnHover: true,
        swipeToSlide: true,
        responsive: [{
                breakpoint: 1400,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    infinite: true
                }
            },
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true
                }
            },
        ]
    });

    $('.slick_sub_category_container').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        pauseOnHover: true,
        autoplaySpeed: 4000,
        slidesToShow: 5,
        slidesToScroll: 5,
        swipeToSlide: true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });

    $('.work-slider').slick({
        dots: true,
        infinite: true,
        speed: 1800,
        autoplay: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        pauseOnHover: true,
        swipeToSlide: true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
        ]
    });

    $('.product-slider').slick({
        dots: true,
        infinite: true,
        pauseOnHover: true,
        speed: 4000,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        pauseOnHover: true,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 1450,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 980,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

});